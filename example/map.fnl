(local autotile (require :autotile))

(local lg love.graphics)
(local {: newQuad : newImage} lg)

(local bitmap-3x3 autotile.bitmap-3x3-simple-alt)
(local bitmap-w 4)

(local map-w 14)
(local tile-size 16)

(local tile-image (newImage :IceTiles3x3-simple.png))

(local tile-data "
00000000000000
00111110110000
00100010110010
00101111111000
00000000110000
01111111111110
01111111111110
00010000110000
00101100011000
00010011100000
00000000000000
")

(local sparse-tiles (autotile.decode tile-data map-w [[:1 bitmap-3x3 bitmap-w]]))

(local quad-regions (autotile.quad-regions (autotile.parse-bitmap bitmap-3x3 4) 4 0 0 16 16 64 64))
(local quads [])

(each [_ {: x : y : w : h : iw : ih} (pairs quad-regions)]
  (table.insert quads (newQuad x y w h iw ih)))

(fn draw-simple []
  (each [_ tile (ipairs sparse-tiles)]
    (when (~= tile.bitmap-index tile.ignore)
      (lg.draw tile-image (. quads tile.bitmap-index)
               (* tile-size (- tile.x 1)) (* tile-size (- tile.y 1)))
      )))

{: draw-simple}
