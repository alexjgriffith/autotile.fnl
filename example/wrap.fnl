(local lg love.graphics)
(local repl (require :stdio))

(local camera {:x 0 :y 0 :scale 3})

(fn love.load []
  (lg.setDefaultFilter :nearest :nearest)
  (lg.setBackgroundColor (/ 79 256) (/ 63 256) (/ 113 256) 1)
  (repl.start))

;; (fn draw-grid [camera w h tw th]
;;   (let [dw (* tw camera.scale)
;;         dh (* th camera.scale)
;;         dx (* (- camera.x tw) camera.scale)
;;         dy (* (- camera.y 1) camera.scale)
;;         nx (+ (/ w camera.scale) 2)
;;         ny (+ (/ h camera.scale) 2)
;;         ])
;;   )

(local (w h) (love.window.getMode))

(fn love.draw []
  (local map (require :map))
  (lg.push)
  ;; (draw-grid camera w h 16 16)
  (lg.scale camera.scale)
  (lg.translate camera.x camera.y)
  (map.draw-simple)
  (lg.pop))

(fn love.update [dt])
