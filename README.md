# Auto Tile
This library is an implementation of [Godot's Autotile](https://docs.godotengine.org/en/stable/tutorials/2d/using_tilemaps.html#autotiles) functionality in fennel for use in love2d. 

This library does not provide an editor for modifying tiles, but can be used as the base representation for tiles. It handles decoding, encoding, autotiling, and setting values in a map.

Refer to to the example to see the how it can be integrated into a love2d project.

The library comes with three built in autotile bitmaps 2x2, 3x3-simple, and 3x3-simple-alt. Users can implement any autotiling bitmaps that suit their tilesets. See the parse-bitmap section below for an example of an autotile bitmap. 

## Key Function Reference
### set-tile
`(autotile.set-tile sparse-map x y w bitmap ?bitmap-index ?ignore)`

### encode
`(autotile.encode sparse-map)`

### decode
`(autotile.decode text w params)`

### re-autotile
`(autotile.re-autotile sparse-map map-w char bitmap bitmap-w)`

### parse-bitmap
`(autotile.parse-bitmap text bitmap-w)`

- text; encoded bitmap
- bitmap-w; the width of the bitmap

Encoded bitmaps are groups of 3x3 characters. 
- `1` indicates that there would be a matching tile on that side or corner
- `0` indicates there will never be a matching tile on that side or corner
- `_` indicates that we're indifference to the presence of a matching tile on that side or corner

If we wanted to make a bitmap that only autotiled when the a tile had no neighbours we could write.

```
000
010
000
```

If we wanted to make a bitmap that only autotiled when the a tile was surrounded by neighbours we could write.

```
111
111
111
```

We can include both of the above bitmaps in one.

```
000111
010111
000111
```

Spaces are ignored by the parsing algorithm. So we can include them between the two tiles for clarity

```
000 111
010 111
000 111
```

Sample encoded bitmap (2x2)
```
"
0_0 0_1 1_0 0_0
_1_ _1_ _1_ _1_
1_0 0_1 1_1 1_1

1_0 0_1 1_1 1_1
_1_ _1_ _1_ _1_
0_1 1_1 1_1 1_0

0_1 1_1 1_1 1_0
_1_ _1_ _1_ _1_
0_0 0_0 0_1 1_0

0_0 0_0 0_1 1_0
_1_ _1_ _1_ _1_
0_0 0_1 1_0 0_0
"
```
Refer to [Godot's Autotile](https://docs.godotengine.org/en/stable/tutorials/2d/using_tilemaps.html#autotiles) documentation for more details.

### quad-regions
`(autotile.quad-regions parsed-bitmap bitmap-w px py pw ph iw ih)`

- parsed-bitmap; the output of parse-bitmap
- bitmap-w; the width of the bitmap
- px; x offset
- py; y offset
- pw; width of the quad
- ph; height of the quad
- iw; width of the source image
- ih: height of the source image

``` fennel
(local autotile (require :autotile))
(local bitmap autotile.bitmap-2x2)
(local bitmap-width 4)
(local parsed-bitmap (autotile.parse-bitmap bitmap) bitmap-width)
(local quad-regions (autotile.quad-regions parsed-butmap bitmap-w 0 0 16 16 64 64))
(local quads [])
(each [_ {: x : y : w : h : iw : ih} (ipairs quads)]
    (table.insert quads (love.graphics.newQuad x y w h iw ih)))

```
