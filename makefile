TITLE="autotile.fnl"
VERSION="0.1.0"
AUTHOR="AlexJGriffith"
DESCRIPTION="An autotiling library"
FENNEL=fennel
LOVE=love
ARGS=--no-metadata

build:
	mkdir -p release
	$(FENNEL) --compile $(ARGS) autotile.fnl > release/autotile.lua

test:
	$(FENNEL) autotile-test.fnl

clean:
	rm -rf release/*.fnl release/*.lua
