(local autotile (require :autotile))

(local sample-encoded-map
"01010
 11011
 01000
 11111
 11111"
)

(local bitmap-2x2 autotile.bitmap-2x2)

(local bitmap-3x3 autotile.bitmap-3x3-simple)

(local bitmap-1 autotile.bitmap-1)

(macro db [text]
  (when true
    `(let [fennel# (require :fennel)]
       (print (fennel#.view ,text)))))

(macro test [name fun comp? result]
  (local t0x01 true)
  (when t0x01 ;; Set to false when distributing
    `(do
       (local fennel# (require :fennel))
       (local comp#
              (or ,comp?
                  (fn [ga# gb#]                           
                    (= (fennel#.view ga#) ( fennel#.view gb#)))))
       (print (fennel#.view
               [,name (if (comp# ,fun ,result)
                          :pass
                          (values :fail ,fun ,result))])))))


(test "grid.set-index"
      (autotile.set-index {} 2 5 :value 1)
      nil
      [nil {:value 1 :x 2 :y 1}])

(test "parse-bitmap"
      (autotile.parse-bitmap bitmap-2x2 4)
      (fn [bitmap _]        
        (and (= 13 (. bitmap 0)) (= (length bitmap) 255)))
      nil)

(test "autotile.parse"
      (autotile.parse sample-encoded-map 5)
      nil
      [:0 :1 :0 :1 :0
       :1 :1 :0 :1 :1
       :0 :1 :0 :0 :0
       :1 :1 :1 :1 :1
       :1 :1 :1 :1 :1])

(test "autotile.nighbours"
      (. (-> sample-encoded-map (autotile.parse 5) (autotile.neighbours 5)) 1)
      nil
      [:0 :_ :_ :_ :1 :1 :1 :_ :_])

(test "autotile.match-keys"
      (. (-> sample-encoded-map
          (autotile.parse 5)
          (autotile.neighbours 5)
          (autotile.match-key 5 "1" "_")
          ) 1)
      _G.=
      "_")

(test "autotile.match-bitmap"
      (-> sample-encoded-map
          (autotile.parse 5)
          (autotile.neighbours 5)
          (autotile.match-key 5 "1" "_")
          (autotile.match-bitmap (autotile.parse-bitmap bitmap-2x2 4))
          )
      nil
      ["NA" 8 "NA" 11 "NA" 7 13 "NA" 13 11 "NA" 3 "NA" "NA" "NA" 7 4 3 4 6 7 7 7 7 7])


(test :autotile.to-sparse (. (-> sample-encoded-map
          (autotile.parse 5)
          (autotile.neighbours 5)
          (autotile.match-key 5 "1" "_")
          (autotile.match-bitmap (autotile.parse-bitmap bitmap-2x2 4))
          (autotile.to-sparse 5 :1 :NA)
          ) 1)
    nil
    {:bitmap "1" :bitmap-index "NA" :ignore "NA" :x 1 :y 1})


(test :autotile.merge
      (let [mapa (-> sample-encoded-map
                     (autotile.parse 5)
                     (autotile.neighbours 5)
                     (autotile.match-key 5 "1" true)
                     (autotile.match-bitmap (autotile.parse-bitmap bitmap-2x2 4))
                     (autotile.to-sparse 5 "1" "NA"))
            
            mapb (-> sample-encoded-map
                     (autotile.parse 5)
                     (autotile.neighbours 5)
                     (autotile.match-key 5 "0" true)
                     (autotile.match-bitmap (autotile.parse-bitmap bitmap-1 1))
                     (autotile.to-sparse 5 "0" "NA"))]
        (autotile.merge mapa mapb)
        )
      nil
[{:bitmap "0" :bitmap-index 1 :ignore "NA" :x 1 :y 1}
 {:bitmap "1" :bitmap-index 8 :ignore "NA" :x 2 :y 1}
 {:bitmap "0" :bitmap-index 1 :ignore "NA" :x 3 :y 1}
 {:bitmap "1" :bitmap-index 11 :ignore "NA" :x 4 :y 1}
 {:bitmap "0" :bitmap-index 1 :ignore "NA" :x 5 :y 1}
 {:bitmap "1" :bitmap-index 7 :ignore "NA" :x 1 :y 2}
 {:bitmap "1" :bitmap-index 13 :ignore "NA" :x 2 :y 2}
 {:bitmap "0" :bitmap-index 1 :ignore "NA" :x 3 :y 2}
 {:bitmap "1" :bitmap-index 13 :ignore "NA" :x 4 :y 2}
 {:bitmap "1" :bitmap-index 11 :ignore "NA" :x 5 :y 2}
 {:bitmap "0" :bitmap-index 1 :ignore "NA" :x 1 :y 3}
 {:bitmap "1" :bitmap-index 3 :ignore "NA" :x 2 :y 3}
 {:bitmap "0" :bitmap-index 1 :ignore "NA" :x 3 :y 3}
 {:bitmap "0" :bitmap-index 1 :ignore "NA" :x 4 :y 3}
 {:bitmap "0" :bitmap-index 1 :ignore "NA" :x 5 :y 3}
 {:bitmap "1" :bitmap-index 7 :ignore "NA" :x 1 :y 4}
 {:bitmap "1" :bitmap-index 4 :ignore "NA" :x 2 :y 4}
 {:bitmap "1" :bitmap-index 3 :ignore "NA" :x 3 :y 4}
 {:bitmap "1" :bitmap-index 4 :ignore "NA" :x 4 :y 4}
 {:bitmap "1" :bitmap-index 6 :ignore "NA" :x 5 :y 4}
 {:bitmap "1" :bitmap-index 7 :ignore "NA" :x 1 :y 5}
 {:bitmap "1" :bitmap-index 7 :ignore "NA" :x 2 :y 5}
 {:bitmap "1" :bitmap-index 7 :ignore "NA" :x 3 :y 5}
 {:bitmap "1" :bitmap-index 7 :ignore "NA" :x 4 :y 5}
 {:bitmap "1" :bitmap-index 7 :ignore "NA" :x 5 :y 5}]
      )

(test :autotile.quad-regions
      (autotile.quad-regions (autotile.parse-bitmap bitmap-2x2 4) 4
                             0 0 16 16 256 256)
      nil
      [{:h 16 :ih 256 :iw 256 :w 16 :x 0 :y 0}
       {:h 16 :ih 256 :iw 256 :w 16 :x 16 :y 0}
       {:h 16 :ih 256 :iw 256 :w 16 :x 32 :y 0}
       {:h 16 :ih 256 :iw 256 :w 16 :x 48 :y 0}
       {:h 16 :ih 256 :iw 256 :w 16 :x 0 :y 16}
       {:h 16 :ih 256 :iw 256 :w 16 :x 16 :y 16}
       {:h 16 :ih 256 :iw 256 :w 16 :x 32 :y 16}
       {:h 16 :ih 256 :iw 256 :w 16 :x 48 :y 16}
       {:h 16 :ih 256 :iw 256 :w 16 :x 0 :y 32}
       {:h 16 :ih 256 :iw 256 :w 16 :x 16 :y 32}
       {:h 16 :ih 256 :iw 256 :w 16 :x 32 :y 32}
       {:h 16 :ih 256 :iw 256 :w 16 :x 48 :y 32}
       {:h 16 :ih 256 :iw 256 :w 16 :x 0 :y 48}
       {:h 16 :ih 256 :iw 256 :w 16 :x 16 :y 48}
       {:h 16 :ih 256 :iw 256 :w 16 :x 32 :y 48}
       {:h 16 :ih 256 :iw 256 :w 16 :x 48 :y 48}])

(local sparse-map (autotile.decode sample-encoded-map 5
                                   [[:1 bitmap-2x2 4] [:0 bitmap-1 1]]))

(db sparse-map)

(test :autotile.decode (autotile.encode sparse-map)
      (fn [s1 s2]
        (= (-> s1
               (string.gsub " " "")
               (string.gsub "\n" ""))
           (-> s2
               (string.gsub " " "")
               (string.gsub "\n" ""))))
      sample-encoded-map)

(db (-> (autotile.set-tile sparse-map 1 1 5 :1)
        (autotile.re-autotile 5 :1 bitmap-2x2 4)))


(local sparse-map-3x3 (autotile.decode sample-encoded-map 5
                                   [[:1 bitmap-3x3 4] [:0 bitmap-1 1]]))

(db sparse-map-3x3)

;; (db (autotile.encode sparse-map))
