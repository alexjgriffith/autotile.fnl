(local autotile {})

(fn autotile.set-index [self i w ...]
  (local xi (+ 1 (% (- i 1) w)))
  (local yi (+ 1 (math.floor (/ (- i 1) w))))
  (local t {:x xi :y yi})
  (local args [...])
  (local l (length args))
  (for [i 1 l 2]
    (tset t (. args i) (. args (+ i 1))))  
  (tset self i t)
  self)

(fn autotile.set [self x y w ...]
  (local i (+ 1 (* w (- y 1)) (- x 1)))
  (local t {:x x :y y})
  (local args [...])
  (local l (length args))
  (for [i 1 l 2]
    (tset t (. args i) (. args (+ i 1))))  
  (tset self i t)
  self)

(fn autotile.set-tile [self x y w bitmap ?bitmap-index ?ignore]
  (autotile.set self x y w
                :bitmap-index (or ?bitmap-index "NA")
                :bitmap bitmap
                :ignore (or ?ignore "NA")))

(tset autotile :bitmap-3x3-simple
"
000 000 000 000
010 011 111 110
010 01_ _1_ _10

010 01_ _1_ _10
010 011 111 110
010 01_ _1_ _10

010 01_ _1_ _10
010 011 111 110
000 000 000 000

000 000 000 000
010 011 111 110
000 000 000 000
")

(tset autotile :bitmap-3x3-simple-alt
"
_0_ _0_ _0_ _0_
010 011 111 110
_1_ _1_ _1_ _1_

_1_ _1_ _1_ _1_
010 011 111 110
_1_ _1_ _1_ _1_

_1_ _1_ _1_ _1_
010 011 111 110
_0_ _0_ _0_ _0_

_0_ _0_ _0_ _0_
010 011 111 110
_0_ _0_ _0_ _0_
")

(tset autotile :bitmap-2x2
"
0_0 0_1 1_0 0_0
_1_ _1_ _1_ _1_
1_0 0_1 1_1 1_1

1_0 0_1 1_1 1_1
_1_ _1_ _1_ _1_
0_1 1_1 1_1 1_0

0_1 1_1 1_1 1_0
_1_ _1_ _1_ _1_
0_0 0_0 0_1 1_0

0_0 0_0 0_1 1_0
_1_ _1_ _1_ _1_
0_0 0_1 1_0 0_0
")

(tset autotile :bitmap-1
"
___
_1_
___
")

(fn n [t i w d]
  (fn check [t i oy ox d]
    (local row (math.floor (/ (+ i oy -1) w)))
    (local index (+ i oy ox))
    (local index-row (math.floor (/ (- index 1) w)))
    (if (and (= index-row row) (. t index))
        (. t index)
        d))
  [(check t i (- w) -1 d) ;; tl
   (check t i (- w) 0  d) ;; t
   (check t i (- w) 1  d) ;; tr
   (check t i 0 1 d)      ;; r
   (check t i w 1 d)      ;; br
   (check t i w 0  d)     ;; b
   (check t i w -1 d)     ;; bl
   (check t i 0 -1 d)])   ;; l

(fn f-n [t i w d]  
  (let [r [(. t i)]
        t2 (n t i w d)]
    (each [k v (ipairs t2)]
      (table.insert r  v))
    r))

(fn autotile.parse-bitmap [text w]
  (let [l (length text)
        t []
        t2 []
        t3 []]
    (for [i 1 l]
      (match (string.sub text i (+ i 0))
        "\n" :skip
        " " :skip
        tile (table.insert t tile)))
    (for [i 1 (length t)]
      (when (and (= 1 (% (math.floor (/ i (* 3 w))) 3))
                 (= 2 (% (math.floor (% i (* 3 w))) 3)))
        (table.insert t2 (n t i (* 3 w) "_"))))
    (each [i list (ipairs t2)]
      (let [r [0]]
        (fn increment-r [k]
          (each [i v (ipairs r)]
            (tset r i (+ v (^ 2 k)))))
        (fn expand-r [k]
          (local l (length r))
          (for [i 1 l]
            (local v (. r i))
            (tset r i (+ v (^ 2 k)))
            (table.insert r v)))
        (each [k l (ipairs list)]
          (match l
            :0 :nil
            :1 (increment-r (- k 1))
            :_  (expand-r (- k 1))))
        (each [_ v (ipairs r)]
          (tset t3 v i))))
    t3))

(fn autotile.parse [text w]
  (let [l (length text)
        t []]
    (for [i 1 l]
      (match (string.sub text i (+ i 0))
        "\n" :skip
        " " :skip
        tile (table.insert t tile)))
    t))

(fn autotile.neighbours [map w]
  (let [t []]
    (each [i value (ipairs map)]
      (table.insert t (f-n map i w "_")))
    t))

(fn autotile.match-key [map w char _?]
  (let [t []]
    (each [_ row (ipairs map)]
      (var j 0)
      (local v (. row 1))
      (each [i value (ipairs row)]
        (when (> i 1)
          (match value
            "_" (when _? (set j (+ j (^ 2 (- i 2)))))
            char (set j (+ j (^ 2 (- i 2))))
            _ :pass
            )))
      (when (~= v char)
          (set j "_"))
      (table.insert t j)
      )
    t))

(fn autotile.match-bitmap [map bitmap name]
  (let [t []]
    (each [_ v (ipairs map)]
      (local o (. bitmap v))
      
      (table.insert t (or o "NA")))
    t))

(fn autotile.to-sparse [map w tileset ignore]
  (let [s {}]
    (each [i v (ipairs map)]
      (autotile.set-index s i w :bitmap-index v
                          :bitmap tileset :ignore ignore))
    s))

(fn autotile.merge [map ...]
  (each [_ m (ipairs [...])]
    (each [k v (ipairs m)]
      (when (= "NA" (. map k :bitmap-index))
        (tset map k v))))
  map)

(fn autotile.quad-regions [bitmap w px py pw ph iw ih]
  (local quad-regions [])
  (each [_ key (pairs bitmap)]
    (tset quad-regions key {:x (+ px (* pw (math.floor (% (- key 1) w))))
                            :y (+ py (* ph (math.floor (/ (- key 1) w))))
                            :w pw
                            :h ph
                            : iw
                            : ih
                            }))
  quad-regions)

(fn autotile.re-autotile [sparse-map map-w char bitmap bitmap-w]
  (let [t []]
    (each [_ {:bitmap value} (ipairs sparse-map)]
      (table.insert t value))    
    (-> t
        (autotile.neighbours map-w)
        (autotile.match-key map-w char true)
        (autotile.match-bitmap (autotile.parse-bitmap bitmap bitmap-w))
        (autotile.to-sparse map-w char "NA")
        (autotile.merge sparse-map)
        )))

(fn autotile.decode [str w params]
  ;; -> params [[char bitmap bitmap-w][char bitmap bitmap-w]]
  (var t false)
  (each [_ [char bitmap bitmap-w] (ipairs  params)]
    (local d
           (-> str
               (autotile.parse w)
               (autotile.neighbours w)
               (autotile.match-key w char true)
               (autotile.match-bitmap (autotile.parse-bitmap bitmap bitmap-w))
               (autotile.to-sparse w char "NA")
               ))
    (if t
        (autotile.merge t d)
        (set t d)
        )
    )
  t)

(fn autotile.encode [sparse-map]
  (var row 1)
  (var ret "")
  (each [key value (ipairs sparse-map)]
    (when (> value.y row)
      (set row value.y)
      (set ret (.. ret "\n"))
      )
    (set ret (.. ret value.bitmap))
    )
  ret)

autotile
